#!/bin/sh

if [ -z ${GIT_NAME} ]; then
  echo -n "Enter github login: "
  read GIT_NAME
fi

if [ -z ${GIT_TOKEN} ]; then
  echo -n "Enter github token: "
  read GIT_TOKEN
fi

docker build --build-arg GIT_NAME=$GIT_NAME --build-arg GIT_TOKEN=$GIT_TOKEN --tag flask-app .

docker-compose up
