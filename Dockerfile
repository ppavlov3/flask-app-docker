FROM python:3.9

WORKDIR /flask-app

ARG GIT_TOKEN
ARG GIT_NAME

ENV TOKEN=${GIT_TOKEN}
ENV NAME=${GIT_NAME}

COPY . /flask-app/

RUN pip install -r requirements.txt

CMD [ "python", "./start.py" ]